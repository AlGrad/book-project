package com.example.Movie2.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Movie2.models.Director;

@Repository
public interface DirectorRepository extends JpaRepository<Director, Long>{
	

}
