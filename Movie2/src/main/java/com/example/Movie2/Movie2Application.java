package com.example.Movie2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class Movie2Application {

	public static void main(String[] args) {
		SpringApplication.run(Movie2Application.class, args);
	}

}
