package com.example.Movie2.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie2.exception.ResourceNotFoundException;
import com.example.Movie2.models.Movie;
import com.example.Movie2.modelsDTO.MovieDTO;
import com.example.Movie2.repositories.MovieRepository;


@RestController
@RequestMapping("/api")
public class MovieController {
	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	MovieRepository movieRepository;
	
	@GetMapping("/movies/readAllDTOMapper")
	public HashMap<String,Object>getAllMovieDTOMapper(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Movie>listMovieEntity = (ArrayList<Movie>) movieRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<MovieDTO>listMovieDTO = new ArrayList<MovieDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Movie movie : listMovieEntity) {
			//inisialisasi obj Publisher DTO 
			MovieDTO movieDTO = modelMapper.map(movie, MovieDTO.class);

			
			listMovieDTO.add(movieDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read All movieDTO Mapp success");
		result.put("Data", listMovieDTO);
		
		return result;
	}
	
	@PostMapping("/movie/createsDTOMapper")
	public HashMap<String, Object> createMovieDTOMapper(@Valid @RequestBody MovieDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Movie movieEntity = modelMapper.map(body, Movie.class);
		//proses saving data ke database
	
		
		movieRepository.save(movieEntity);
		
		body.setId(movieEntity.getId());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new movieDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	
	@PutMapping("/mpvie/updateDTOMapper/{id}")
	public HashMap<String, Object> updateMovieDTOMapper(@PathVariable(value = "id") Long id, @Valid @RequestBody MovieDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Movie movieEntity = movieRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Movie", "id", id));
		
		
		movieEntity = modelMapper.map(body, Movie.class);
		
		movieEntity.setId(id);
	
		
		movieRepository.save(movieEntity);
		
		body = modelMapper.map(movieEntity, MovieDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update new movieDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	@DeleteMapping("/movie/deleteDTOMapper/{id}")
	public HashMap<String, Object>deleteMovieDTOMapper(@PathVariable(value = "id")Long id,  MovieDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Movie movieEntity = movieRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Movie", "id", id));
		
		movieEntity = modelMapper.map(body, Movie.class);
		movieEntity.setId(id);
		
		movieRepository.delete(movieEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete movieDTO Mapp success");
		result.put("Data", "[]");
		
		return result;
		
	}

}
