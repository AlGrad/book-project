package com.example.Movie2.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie2.exception.ResourceNotFoundException;
import com.example.Movie2.models.Director;
import com.example.Movie2.modelsDTO.DirectorDTO;
import com.example.Movie2.repositories.DirectorRepository;

@RestController
@RequestMapping("/api")
public class DirectorController {
	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	DirectorRepository directorRepository;
	
	@GetMapping("/directors/readAllDTOMapper")
	public HashMap<String,Object>getAllDirectorDTOMapper(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Director>listDirectorEntity = (ArrayList<Director>) directorRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<DirectorDTO>listDirectorDTO = new ArrayList<DirectorDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Director director : listDirectorEntity) {
			//inisialisasi obj Publisher DTO 
			DirectorDTO directorDTO = modelMapper.map(director, DirectorDTO.class);

			
			listDirectorDTO.add(directorDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read All directorDTO Mapp success");
		result.put("Data", listDirectorDTO);
		
		return result;
	}
	
	@PostMapping("/director/createsDTOMapper")
	public HashMap<String, Object> createDirectorDTOMapper(@Valid @RequestBody DirectorDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Director directorEntity = modelMapper.map(body, Director.class);
		//proses saving data ke database
	
		
		directorRepository.save(directorEntity);
		
		body.setId(directorEntity.getId());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new directorDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	
	@PutMapping("/director/updateDTOMapper/{id}")
	public HashMap<String, Object> updateDirectorDTOMapper(@PathVariable(value = "id") Long id, @Valid @RequestBody DirectorDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Director directorEntity = directorRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Director", "id", id));
		
		
		directorEntity = modelMapper.map(body, Director.class);
		
		directorEntity.setId(id);
	
		
		directorRepository.save(directorEntity);
		
		body = modelMapper.map(directorEntity, DirectorDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update new directorDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	@DeleteMapping("/director/deleteDTOMapper/{id}")
	public HashMap<String, Object>deleteDirectorDTOMapper(@PathVariable(value = "id")Long id,  DirectorDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Director directorEntity = directorRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Director", "id", id));
		
		directorEntity = modelMapper.map(body, Director.class);
		directorEntity.setId(id);
		
		directorRepository.delete(directorEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete directorDTO Mapp success");
		result.put("Data", "[]");
		
		return result;
		
	}

}
