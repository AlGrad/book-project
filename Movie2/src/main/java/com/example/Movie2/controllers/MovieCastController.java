package com.example.Movie2.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie2.exception.ResourceNotFoundException;
import com.example.Movie2.models.Movie;
import com.example.Movie2.models.MovieCast;
import com.example.Movie2.models.MovieCastId;
import com.example.Movie2.modelsDTO.MovieCastDTO;
import com.example.Movie2.repositories.MovieCastRepository;

@RestController
@RequestMapping("/api")
public class MovieCastController {
	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	MovieCastRepository movieCastRepository;
	
	@GetMapping("/movCast/readAllDTOMapper")
	public HashMap<String,Object>getAllMovieCastDTOMapper(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<MovieCast>listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<MovieCastDTO>listMovieCastDTO = new ArrayList<MovieCastDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(MovieCast movieCast : listMovieCastEntity) {
			//inisialisasi obj Publisher DTO 
			MovieCastDTO movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);

			
			listMovieCastDTO.add(movieCastDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read All movieCastDTO Mapp success");
		result.put("Data", listMovieCastDTO);
		
		return result;
	}
	
	@PostMapping("/movCast/createsDTOMapper")
	public HashMap<String, Object> createMovieCastDTOMapper(@Valid @RequestBody MovieCastDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		boolean isTrue = false;
		
		ArrayList<MovieCast> listMovCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		MovieCast movieCastEntity = modelMapper.map(body, MovieCast.class);
		
		for(MovieCast movieCast : listMovCastEntity) {
			if(movieCast.getId().getActId() == body.getId().getActId() && movieCast.getId().getMovId() == body.getId().getMovId() ) {
				
				
				isTrue = true;
				
				
				
			}else {
				isTrue = false;
				
				movieCastRepository.save(movieCastEntity);
				
				body.setId(movieCastEntity.getId());
				
			}	
				
			if(isTrue == true) {
				result.put("Status", "actId and movId is reaady used ");
				
			}else if (isTrue == false) {
				
				result.put("Status", 200);
				result.put("Message", "Update new movCastDTO Mapp success");
				result.put("Data", body);
			}
		}
	
		
		
		return result;
	}
	
	@PutMapping("/movCast/updateDTOMapper/{actId}/{movId}")
	public HashMap<String, Object> updateMovCastDTOMapper(@PathVariable(value = "actId")long actId, @PathVariable(value = "movId")long movId,@Valid @RequestBody MovieCastDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		boolean isTrue = false;
//		MovieCastId movieCastId = new MovieCastId();
//		movieCastId.setActId(actId);
//		movieCastId.setMovId(movId);
		
		ArrayList<MovieCast> listMovCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
//		MovieCast movieCastEntity = new MovieCast();
		
		
		for(MovieCast movieCast : listMovCastEntity) {
			if(movieCast.getId().getActId() == actId && movieCast.getId().getMovId() == movId) {
				
				
				movieCast.setRole(body.getRole());
				
				movieCastRepository.save(movieCast);
				
				body = modelMapper.map(movieCast, MovieCastDTO.class);
				
				isTrue = true;
				
				
				
			}else {
				isTrue = false;
				
			}	
				
			if(isTrue == true) {
				result.put("Status", 200);
				result.put("Message", "Update new movCastDTO Mapp success");
				result.put("Data", body);
			}else if (isTrue == false) {
				result.put("Status", "Not true");
				
			}
		}
		
		
		
		return result;
	
	}
	
	@DeleteMapping("/movCast/deleteDTOMapper/{actId}/{movId}")
	public HashMap<String, Object>deleteMovCastDTOMapper(@PathVariable(value = "actId")long actId, @PathVariable(value = "movId")long movId,  MovieCastDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		boolean isTrue = false;
		
		ArrayList<MovieCast> listMovCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		for(MovieCast movieCast : listMovCastEntity) {
			if(movieCast.getId().getActId() == actId && movieCast.getId().getMovId() == movId) {
				
				movieCastRepository.delete(movieCast);
				
				body = modelMapper.map(movieCast, MovieCastDTO.class);
				
				isTrue = true;
				
				
				
			}else {
				isTrue = false;
				
			}	
		}
		
		result.put("Status", 200);
		result.put("Message", "Delete movCastDTO Mapp success");
		result.put("Data", "[]");
		
		return result;
		
	}
	
}
