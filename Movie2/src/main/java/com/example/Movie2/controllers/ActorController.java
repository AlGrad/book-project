package com.example.Movie2.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie2.models.Actor;
import com.example.Movie2.repositories.ActorRepository;
import com.example.Movie2.modelsDTO.ActorDTO;
import com.example.Movie2.exception.ResourceNotFoundException;;

@RestController
@RequestMapping("/api")
public class ActorController {
	
	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	ActorRepository actorRepository;
	
	@GetMapping("/actors/readAllDTOMapper")
	public HashMap<String,Object>getAllActorDTOMapper(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Actor>listActorEntity = (ArrayList<Actor>) actorRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<ActorDTO>listActorDTO = new ArrayList<ActorDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Actor actor : listActorEntity) {
			//inisialisasi obj Publisher DTO 
			ActorDTO actorDTO = modelMapper.map(actor, ActorDTO.class);

			
			listActorDTO.add(actorDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read All actor Mapp success");
		result.put("Data", listActorDTO);
		
		return result;
	}
	
	@PostMapping("/actor/createsDTOMapper")
	public HashMap<String, Object> createActorDTOMapper(@Valid @RequestBody ActorDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Actor actorEntity = modelMapper.map(body, Actor.class);
		//proses saving data ke database
	
		
		actorRepository.save(actorEntity);
		
		body.setId(actorEntity.getId());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new actorDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	
	@PutMapping("/actor/updateDTOMapper/{id}")
	public HashMap<String, Object> updateActorDTOMapper(@PathVariable(value = "id") Long id, @Valid @RequestBody ActorDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Actor actorEntity = actorRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Actor", "id", id));
		
		
		actorEntity = modelMapper.map(body, Actor.class);
		
		actorEntity.setId(id);;
	
		
		actorRepository.save(actorEntity);
		
		body = modelMapper.map(actorEntity, ActorDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update new actorDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	@DeleteMapping("/actor/deleteDTOMapper/{id}")
	public HashMap<String, Object>deleteActorDTOMapper(@PathVariable(value = "id")Long id,  ActorDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Actor actorEntity = actorRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Actor", "id", id));
		
		actorEntity = modelMapper.map(body, Actor.class);
		actorEntity.setId(id);
		
		actorRepository.delete(actorEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete actorDTO Mapp success");
		result.put("Data", "[]");
		
		return result;
		
	}

}
