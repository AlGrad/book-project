package com.example.Movie2.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie2.exception.ResourceNotFoundException;
import com.example.Movie2.models.Genre;
import com.example.Movie2.modelsDTO.GenreDTO;
import com.example.Movie2.repositories.GenreRepository;

@RestController
@RequestMapping("/api")
public class GenreController {
	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	GenreRepository genreRepository;
	
	@GetMapping("/genres/readAllDTOMapper")
	public HashMap<String,Object>getAllGenreDTOMapper(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Genre>listGenreEntity = (ArrayList<Genre>) genreRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<GenreDTO>listGenreDTO = new ArrayList<GenreDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Genre genre : listGenreEntity) {
			//inisialisasi obj Publisher DTO 
			GenreDTO genreDTO = modelMapper.map(genre, GenreDTO.class);

			
			listGenreDTO.add(genreDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read All genreDTO Mapp success");
		result.put("Data", listGenreDTO);
		
		return result;
	}
	
	@PostMapping("/genre/createsDTOMapper")
	public HashMap<String, Object> createGenreDTOMapper(@Valid @RequestBody GenreDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Genre genreEntity = modelMapper.map(body, Genre.class);
		//proses saving data ke database
	
		
		genreRepository.save(genreEntity);
		
		body.setId(genreEntity.getId());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new genreDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	
	@PutMapping("/genre/updateDTOMapper/{id}")
	public HashMap<String, Object> updateGenreDTOMapper(@PathVariable(value = "id") Long id, @Valid @RequestBody GenreDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Genre genreEntity = genreRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Genre", "id", id));
		
		
		genreEntity = modelMapper.map(body, Genre.class);
		
		genreEntity.setId(id);
	
		
		genreRepository.save(genreEntity);
		
		body = modelMapper.map(genreEntity, GenreDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update new genreDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	@DeleteMapping("/genre/deleteDTOMapper/{id}")
	public HashMap<String, Object>deleteGenreDTOMapper(@PathVariable(value = "id")Long id,  GenreDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Genre genreEntity = genreRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Genre", "id", id));
		
		genreEntity = modelMapper.map(body, Genre.class);
		genreEntity.setId(id);
		
		genreRepository.delete(genreEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete genreDTO Mapp success");
		result.put("Data", "[]");
		
		return result;
		
	}

}
